package com.courses.tellus.config.spring;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
            final AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
            webCtx.register(WebMvcConfig.class);
            webCtx.setServletContext(servletContext);

            final ServletRegistration.Dynamic servlet = servletContext
                    .addServlet("dispatcher", new DispatcherServlet(webCtx));
            servlet.setLoadOnStartup(1);
            servlet.addMapping("/");
    }
}
