package com.courses.tellus.config.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@EnableWebMvc
@Configuration
@ComponentScan({"com.courses.tellus"})
public class WebMvcConfig implements WebMvcConfigurer {

    private static final String INDEX_PAGE = "forward:/index.jsp";
    //private static final String LOGIN_PAGE = "forward:/login_page.jsp";

    /**
     * Provide configuration for views.
     * @return ViewResolver
     */
    @Bean
    public InternalResourceViewResolver setupViewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/spring/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName(INDEX_PAGE);
        //registry.addViewController("/login").setViewName(LOGIN_PAGE);
    }
}