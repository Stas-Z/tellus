package com.courses.tellus.service.mvc;

import java.util.List;
import java.util.Optional;

import com.courses.tellus.entity.model.Student;

public interface StudentService {
    /**
     * Method for getting all universities from database.
     *
     * @return list of universities or empty list
     */

    List<Student> getAll();

    /**
     * Method for getting university from DB by id.
     *
     * @param uniId id of the university to select from DB
     * @return an Optional with a present value if the specified value
     *         is non-null, otherwise an empty Optional
     */

    Optional<Student> getById(final Long uniId);

    /**
     * Method for inserting new university into database.
     *
     * @param student  for inserting
     * @return number of affected rows int database
     */

    int insert(final Student student);

    /**
     * Method for deleting university from database by id.
     *
     * @param uniId id of the university to remove from database
     * @return number of affected rows in database
     */

    int delete(final Long uniId);

    /**
     * Method for updatingting university from database by id.
     *
     * @param student to update
     * @return number of affected rows in database
     */

    int update(final Student student);
}
