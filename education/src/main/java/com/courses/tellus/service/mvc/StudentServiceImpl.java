package com.courses.tellus.service.mvc;

import java.util.List;
import java.util.Optional;

import com.courses.tellus.entity.model.Student;
import com.courses.tellus.persistence.dao.spring.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

@Service
@ComponentScan("com.courses.tellus.persistence.dao.spring.jdbc")
public class StudentServiceImpl implements StudentService {

    @Autowired
    private transient StudentDao studentDao;

    @Override
    public List<Student> getAll() {
        return studentDao.getAll();
    }

    @Override
    public Optional<Student> getById(final Long studentId) {
        return studentDao.getById(studentId);
    }

    @Override
    public int insert(final Student student) {
        return studentDao.insert(student);
    }

    @Override
    public int delete(final Long studentId) {
        return studentDao.delete(studentId);
    }

    @Override
    public int update(final Student student) {
        return studentDao.update(student);
    }
}
