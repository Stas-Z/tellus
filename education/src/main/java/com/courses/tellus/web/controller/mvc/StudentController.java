package com.courses.tellus.web.controller.mvc;

import com.courses.tellus.entity.model.Student;
import com.courses.tellus.service.mvc.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/spring/mvc/student")
public class StudentController {

    @Autowired
    private transient StudentService serviceImpl;


    /**
     * This method forwards List of universities to university_list.jsp.
     *
     * @param model - Model.
     * @return - name of jsp
     */
    @GetMapping("/list")
    public String getAllStudents(final Model model) {
        model.addAttribute("studentList", serviceImpl.getAll());
        return "student_list";
    }

    /**
     * Method provide view for creating new university.
     *
     * @return view "student_create.jsp"
     */
    @GetMapping("/add")
    public String addStudent() {
        return "student_create";
    }

    /**
     * Method forwards attributes from createUniversity.jsp to DB.
     *
     * @return - name of jsp
     */
    @PostMapping("/add")
    public String addStudent(@ModelAttribute("student") final Student student) {
        serviceImpl.insert(student);
        return "redirect:/spring/mvc/student/list";
    }

    /**
     * @param studentId from Http request.
     * @return - name of jsp
     */
    @GetMapping("/delete/{studentId}")
    public String deleteStudent(@PathVariable("studentId") final Long studentId) {
        serviceImpl.delete(studentId);
        return "redirect:/spring/mvc/student/list";
    }

    /**
     * @param studentId from Http request.
     * @return - name of jsp
     */
    @GetMapping("/edit/{studentId}")
    public String updateStudent(@PathVariable("studentId") final Long studentId, final Model model) {
        model.addAttribute("student", serviceImpl.getById(studentId).get());
        return "student_edit";
    }

    /**
     * Method forwards attributes from createUniversity.jsp to DB.
     *
     * @return - name of jsp
     */

    @PostMapping("/edit")
    public String updateStudent(@ModelAttribute("student") final Student student) {
        serviceImpl.update(student);
        return "redirect:/spring/mvc/student/list";
    }

}
