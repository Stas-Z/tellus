-- Creating table block
  -- Universities table
DROP TABLE IF EXISTS University;
CREATE TABLE IF NOT EXISTS University(
  univer_id INT(10) not null primary key auto_increment,
  name_of_university VARCHAR (100) NOT NULL,
  address VARCHAR (100),
  specialization VARCHAR(75)
);

  -- Students table
DROP TABLE IF EXISTS Student;
CREATE TABLE IF NOT EXISTS Student (
  student_id INT(10) not null primary key auto_increment,
  first_name VARCHAR(20) NOT NULL,
  last_name VARCHAR(20) NOT NULL,
  student_card_number  VARCHAR(20),
  address VARCHAR(255) NOT NULL
);

	-- Subject table
DROP TABLE IF EXISTS Subject;
create table IF NOT EXISTS Subject (
	subject_id INT(10) not null primary key auto_increment,
  name varchar(50) not null,
  descr VARCHAR(255) not null,
  valid  BOOLEAN not null,
  date_of_creation date not null
);






