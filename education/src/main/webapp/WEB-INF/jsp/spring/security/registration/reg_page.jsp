<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Registration Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <style>
        <%@ include file="/WEB-INF/jsp/spring/security/registration/reg_style.css" %>
    </style>
</head>
<body style='margin:50px;'>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <p class="form-title">
                    Registration</p>
                <c:if test="${not empty message}">
                    <div class="col-4 mx-auto">
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p class="text-center">${message}</p>
                        </div>
                    </div>
                </c:if>
                <form class="reg" action="/reg" method="post">
                    <input type="text" id="username" name="username" placeholder="Username"
                           value="${user.username}"/>
                    <input type="password" id="password" name="password" placeholder="Password" ${user.password}/>
                    <input type="password" id="re-password" name="repassword" placeholder="Repeat password"
                    ${user.repassword}/>
                    <input type="submit" value="Register" class="btn btn-success btn-sm" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
