<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>EDUCATION - Student creation</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="in_nav.jsp" />

<div class="container col-5 text-center ">
	<hr>
	<form class="form-horizontal " action="/spring/mvc/student/add" method="POST">

		<!--first name -->
		<div class="form-group container text-left">
			<label class="control-label col-md-3">First name</label>
			<div class="col-md-11">
				<input type="text" class="form-control" name="firstName"/>
			</div>
		</div>

		<!--last name -->
		<div class="form-group  container text-left">
		<label class="control-label col-md-3">Last name</label>
		<div class="col-md-11">
			<input type="text" class="form-control" name="lastName"/>
		</div>
		</div>

		<!-- studentCardNumber -->

		<div class="form-group col-md-12 row">
			<div class="col-md-6 text-left ">
		<label class="control-label col-md-6">Card number</label>
		<div class="col-md-12">
			<input type="text" class="form-control" name="studentCardNumber"/>
		</div>
			</div>
		<!-- address -->
			<div class="col-md-6 text-left ">
			<label class="control-label col-md-6">Address</label>
			<div class="col-md-11">
				<textarea type="text" class="form-control" name="address"></textarea>
			</div>
		</div>
		</div>
		<hr>
		<div class="form-group container">
			<input type="submit" class="btn btn-primary btn-lg " value="  Save  "/>
		</div>
	</form>
</div>
</body>
</html>