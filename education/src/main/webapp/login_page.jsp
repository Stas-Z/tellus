<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta charset="utf-8">
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <style>
        <%@ include file="/WEB-INF/jsp/spring/security/login/login_style.css" %>
    </style>
</head>
<body style='margin:50px;'>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="wrap">
                    <p class="form-title">
                        Sign In</p>
                    <form class="login" action="/login" method="post">
                        <c:if test="${param.error != null}">
                            <p style='color:red'>
                                Invalid username and password.
                            </p>
                        </c:if>
                        <c:if test="${param.logout != null}">
                            <p style='color:blue'>
                                You have been logged out.
                            </p>
                        </c:if>
                        <input type="text" id="username" name="username" placeholder="Username" />
                        <input type="password" id="password" name="password" placeholder="Password" />
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input type="submit" value="Sign In" class="btn btn-success btn-sm" />
                        <a href="/reg">
                            <input type="button" value="Registry" class="btn btn-inline-warning btn-sm" />
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
