package com.courses.tellus.service.mvc;

import com.courses.tellus.entity.model.Student;
import com.courses.tellus.persistence.dao.spring.StudentDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;


public class StudentServiceImplMockTest {

    @Mock
    private StudentDao studentDao;

    @Mock
    private Student student;

    @InjectMocks
    private StudentServiceImpl studentServiceImpl;

    @BeforeEach
     void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
     void testInsertUniversity() {
        given(student.getFirstName()).willReturn("testFN");
        given(student.getLastName()).willReturn("testLN");
        given(student.getStudentCardNumber()).willReturn("testSC");
        given(student.getAddress()).willReturn("testA");
        given(studentDao.insert(any())).willReturn(1);

        assertEquals(1, studentServiceImpl.insert(student));
        verify(studentDao, atLeastOnce()).insert(any());
    }

    @Test
    void testUpdateUniversity() {
        given(student.getFirstName()).willReturn("testFN");
        given(student.getLastName()).willReturn("testLN");
        given(student.getStudentCardNumber()).willReturn("testSC");
        given(student.getAddress()).willReturn("testA");
        given(studentDao.update(any())).willReturn(1);

        assertEquals(1, studentServiceImpl.update(student));
        verify(studentDao, atLeastOnce()).update(any());
    }

    @Test
    void testDeleteUniversity(){
        given(studentDao.delete(anyLong())).willReturn(1);

        assertEquals(1, studentServiceImpl.delete(anyLong()));
        verify(studentDao, atLeastOnce()).delete(anyLong());
    }

    @Test
    void testGetById(){
            given(studentServiceImpl.getById(anyLong())).willReturn(Optional.of(student));
            assertTrue(studentServiceImpl.getById(anyLong()).isPresent());
            verify(studentDao, atLeastOnce()).getById(anyLong());
    }

        @Test
    void testGetAll(){
            List<Student> universities = new ArrayList<>();
            universities.add(student);
            given(studentServiceImpl.getAll()).willReturn(universities);

            assertEquals(1, studentServiceImpl.getAll().size());
            verify(studentDao, atLeastOnce()).getAll();
    }
}
