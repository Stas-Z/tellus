package com.courses.tellus.web.controller.mvc;


import com.courses.tellus.entity.model.Student;
import com.courses.tellus.entity.model.University;
import com.courses.tellus.service.mvc.StudentService;
import com.courses.tellus.service.mvc.UniversityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

public class StudentControllerMockTest {

    @Mock
    private Student student;

    @Mock
    private Model model;

    @Mock
    private StudentService studentServiceImpl;

    @InjectMocks
    private StudentController studentController;

    @BeforeEach
    public void setUp() {
        studentController = new StudentController();
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testGetAllStudents() {
        List<Student> students = new ArrayList<>();
        students.add(student);
        when(studentServiceImpl.getAll()).thenReturn(students);
        assertEquals("student_list", studentController.getAllStudents(model));
    }

    @Test
    public void  testAddStudentMethodPost() {
        when(studentServiceImpl.insert(anyObject())).thenReturn(1);
        assertEquals("redirect:/spring/mvc/student/list", studentController.addStudent(student));
    }

    @Test
    public void  testAddStudentMethodGet(){
        assertEquals("student_create", studentController.addStudent());
    }

    @Test
    public void testUpdateStudentMethodPost() {
        when(studentServiceImpl.update(anyObject())).thenReturn(1);
        assertEquals("redirect:/spring/mvc/student/list", studentController.updateStudent(student));
    }

    @Test
    public void testUpdateAutoMethodGet() {
        when(studentServiceImpl.getById(anyLong())).thenReturn(Optional.of(student));
        assertEquals("student_edit", studentController.updateStudent(anyLong(), model));
    }

    @Test
    public void testDeleteStudent() {
        when(studentServiceImpl.delete(anyLong())).thenReturn(1);
        assertEquals("redirect:/spring/mvc/student/list", studentController.deleteStudent(anyLong()));
    }

}

