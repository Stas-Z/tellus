package com.courses.tellus.web.controller.rest;

import com.courses.tellus.entity.dto.StudentDto;
import com.courses.tellus.entity.model.Student;
import com.courses.tellus.service.rest.StudentRestServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;


public class StudentRestControllerMockTest {

    @Mock private StudentRestServiceImpl service;
    @Mock private StudentDto studentDto;
    @Mock private Student student;
    @Mock private BindingResult bindingResult;
    @Mock private FieldError fieldError;

    @InjectMocks StudentRestController restController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testFindAllAndItSuccessful(){
        List<Student> students = new ArrayList<>();
        students.add(student);
        given(service.getAll()).willReturn(students);

        assertEquals(ResponseEntity.ok(students), restController.getAll());
    }

    @Test
    void testFindAllAndThrowException(){
        List<Student> students = new ArrayList<>();
        students.add(student);
        given(service.getAll()).willThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () ->  restController.getAll());
    }

    @Test
    void testFindByIdAndItSuccessful() {
        given(service.getEntityById(anyLong())).willReturn(Optional.of(student));

        assertEquals(ResponseEntity.ok(student), restController.getById(anyLong()));
    }

    @Test
    void testFindByIdAndThrowException() {
        given(service.getEntityById(anyLong())).willThrow(IllegalArgumentException.class);

        assertThrows(IllegalArgumentException.class, () ->  restController.getById(anyLong()));
    }

    @Test
    void testDeleteAndItSuccessful() {
            restController.delete(anyLong());
            verify(service, atLeastOnce()).delete(anyLong());
        }


    @Test
    void testInsertAndItSuccessful() {
        restController.insert(studentDto, bindingResult);
        verify(service, atLeastOnce()).insert(studentDto);
    }

    @Test
    void testInsertAndWhenValidationIsFailed() {
        ResponseEntity<String> expected = new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
        given(bindingResult.hasErrors()).willReturn(true);
        given(bindingResult.getFieldError()).willReturn(fieldError);
        given(fieldError.getDefaultMessage()).willReturn("error");
        assertEquals(expected,restController.insert(studentDto, bindingResult));
    }
    @Test
    void testUpdateAndItSuccessful() {
        given(service.update(1L, studentDto)).willReturn(true);

        assertEquals(ResponseEntity.ok(null), restController.update(1L, studentDto, bindingResult));
    }
    @Test
    void testUpdateAndWhenValidationIsFailed() {
        ResponseEntity<String> expected = new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
        final Long idForTest = 2L;
        given(bindingResult.hasErrors()).willReturn(true);
        given(bindingResult.getFieldError()).willReturn(fieldError);
        given(fieldError.getDefaultMessage()).willReturn("error");
        assertEquals(expected,restController.update(idForTest,studentDto, bindingResult));
    }



    @Test
    void exceptionCatchMainTest() {
        assertEquals(HttpStatus.BAD_REQUEST, restController.exceptionCatchMain().getStatusCode());
    }

    @Test
    void unsupportedMediaTypeCatchTest() {
        assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE, restController.unsupportedMediaTypeCatch().getStatusCode());
    }
}